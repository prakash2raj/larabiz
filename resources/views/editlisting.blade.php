@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Edit Listing
                    <a href="/dashboard" class="btn btn-outline-success btn-sm">Go Back</a>
                </div>

                <div class="card-body">
                    @if(isset($listing))
                    {!! Form::open(['action' => ['ListingsController@update', $listing->id], 'method' => 'POST']) !!}
                    <div class="form-group">
                        {{ Form::label('name', 'Company Name', ['class' => 'sr-only']) }}
                        {{ Form::text('name', $listing->name, ['id' => 'name', 'class' => 'form-control', 'placeholder' => 'Your Company Name']) }}

                        @if($errors->has('name'))
                        <p class="alert alert-danger">
                            {{$errors->first('name')}}
                        </p>
                        @endif
                    </div>
                    <div class="form-group">
                        {{ Form::label('address', 'Address', ['class' => 'sr-only']) }}
                        {{ Form::text('address', $listing->address, ['id' => 'address', 'class' => 'form-control', 'placeholder' => 'Your Address']) }}

                        @if($errors->has('address'))
                        <p class="alert alert-danger">
                            {{$errors->first('address')}}
                        </p>
                        @endif
                    </div>
                    <div class="form-group">
                        {{ Form::label('website', 'Website', ['class' => 'sr-only']) }}
                        {{ Form::url('website', $listing->website, ['id' => 'website', 'class' => 'form-control', 'placeholder' => 'Your Website']) }}

                        @if($errors->has('website'))
                        <p class="alert alert-danger">
                            {{$errors->first('website')}}
                        </p>
                        @endif
                    </div>
                    <div class="form-group">
                        {{ Form::label('email', 'Email', ['class' => 'sr-only']) }}
                        {{ Form::email('email', $listing->email, ['id' => 'email', 'class' => 'form-control', 'placeholder' => 'Your Email']) }}

                        @if($errors->has('email'))
                        <p class="alert alert-danger">
                            {{$errors->first('email')}}
                        </p>
                        @endif
                    </div>
                    <div class="form-group">
                        {{ Form::label('phone', 'Phone', ['class' => 'sr-only']) }}
                        {{ Form::text('phone', $listing->phone, ['id' => 'phone', 'class' => 'form-control', 'placeholder' => 'Your Phone']) }}

                        @if($errors->has('phone'))
                        <p class="alert alert-danger">
                            {{$errors->first('phone')}}
                        </p>
                        @endif
                    </div>
                    <div class="form-group">
                        {{ Form::label('bio', 'Bio', ['class' => 'sr-only']) }}
                        {{ Form::textarea('bio', $listing->bio, ['id' => 'bio', 'class' => 'form-control', 'placeholder' => 'Your Bio', 'rows' => 3]) }}

                        @if($errors->has('bio'))
                        <p class="alert alert-danger">
                            {{$errors->first('bio')}}
                        </p>
                        @endif
                    </div>
                    {{ Form::hidden('_method', 'PUT') }}

                    {{ Form::submit('Update',['class' => 'btn btn-primary btn-block']) }}

                    {!! Form::close() !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection