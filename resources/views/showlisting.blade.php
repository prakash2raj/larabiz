@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @include('inc.messages')
            <div class="card">
                <div class="card-header">
                    <a href="{{(isset(auth()->user()->id) ? '/dashboard' : '/')}}" class="btn btn-outline-primary btn-sm pull-right">Go Back</a>
                    
                </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    @if(isset($listing))

                    <table class="table">
                        <tr>
                            <th>Company</th>
                            <td>{{$listing->name}}</td>
                        </tr>
                        <tr>
                            <th>Address</th>
                            <td>{{$listing->address}}</td>
                        </tr>
                        <tr>
                            <th>Website</th>
                            <td><a href="{{$listing->website}}">{{$listing->website}}</a></td>
                        </tr>
                        <tr>
                            <th>Phone</th>
                            <td>{{$listing->phone}}</td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td>{{$listing->email}}</td>
                        </tr>
                        <tr>
                            <th>Bio</th>
                            <td>{{$listing->bio}}</td>
                        </tr>
                    </table>

                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection