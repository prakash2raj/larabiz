@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @include('inc.messages')
            <div class="card">
                <div class="card-header">Your Listings <a href="/listings/create" class="btn btn-outline-success btn-sm">Create</a></div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    @if(count($listings) > 0)
                    <div class="row">

                        @foreach($listings as $listing)
                        <div class="col-md-6 col-lg-4 col-sm-6 col-xs-12 mb-2">
                            <div class="card">
                                <div class="card-header">
                                    <a href="/listings/{{$listing->id}}">{{$listing->name}}</a>
                                </div>
                                <div class="card-body">
                                    <span class="badge badge-success">{{$listing->address}}</span>
                                    <br><a class="badge badge-warning" href="{{$listing->website}}" target="_blank">{{$listing->website}}</a>
                                </div>
                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col-md-6 col-lg-6">
                                            <a href="/listings/{{$listing->id}}/edit" class="btn btn-outline-success btn-sm">Edit</a>
                                        </div>
                                        <div class="col-md-6 col-lg-6">
                                            {!! Form::open(['action' => ['ListingsController@destroy', $listing->id], 'method' => 'POST', 'onsubmit' => 'return confirm("Are you sure?")', 'class' => 'text-right']) !!}
                                            {{ Form::hidden('_method', 'DELETE')}}
                                            {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @endforeach
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection